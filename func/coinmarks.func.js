const router    = global.express.Router();
const coinmarks = true && require('coinmarks') || require('../../../_PACKS/coinmarks');
const fs        = require('fs');
//const content   = require('../files/content.json');
const lodash    = require('lodash');
const fetch     = require('node-fetch');
const firebase  = require('firebase');

const db = firebase.initializeApp({
  apiKey:       'AIzaSyA2EH6q5S1PAeFfpFesXt_dgZKwOrZy1NQ',
  authDomain:   'coinmarks-e92c0.firebaseapp.com',
  databaseURL:  'https://coinmarks-e92c0.firebaseio.com/',
}).database().ref('/');

// ROUTES
router.get('/',                 (req, res) => getCoinStats().then(obj => res.json(obj)));
router.get('/update',           (req, res) => update().then(obj => res.json(obj)));
router.get('/stored/:col',      (req, res) => getStored(null, req.params.col).then(arr => res.json(arr)));
router.get('/missing/coins',    (req, res) => getMissingLatestCoins().then(obj => res.json(obj)));
router.get('/missing/extras/:limit?/:full?',   (req, res) => getMissingCoinExtras(req.params.limit, req.params.full).then(arr => res.json(arr)));
router.get('/fieldless/:field', (req, res) => getFieldlessCoins(req.params.field).then(arr => res.json(arr)));
router.get('/test',             (req, res) => coinmarks.latest().then(arr => res.json(arr)));
router.get('/set/:limit?',      (req, res) => setMissingCoinExtras(req.params.limit || 5).then(arr => {
  arr.length > 0
    ? setTimeout( function(){ res.redirect('/coinmarks/set/' + ranum(1, 10)) }, ranum(1000, 3000)) 
    : res.json(arr)
}));

module.exports = router;

const TICK_DELAY  = 1000 * 60 * 5;
const LAST_UPDATE = { timestamp: null };
// METHODS

//WRITES
function update(){
  if(LAST_UPDATE.timestamp > new Date().getTime())
    return Promise.resolve(new Response())
    
  return Promise.all([
    getMissingLatestCoins().then( coins => Promise.all(coins.map(coin => updateStored(new Coin(coin)))) ),
    getMissingSupplyCoins().then( coins => Promise.all(coins.map(coin => updateStored(new Coin(coin)))) ),
    getMissingListedCoins().then( coins => Promise.all(coins.map(coin => updateStored(new Coin(coin)))) )
  ])
    .then( ([latest, supply, listed]) => {
      LAST_UPDATE.timestamp = new Date().getTime() + TICK_DELAY;
      return new Response({ latest, supply, listed })
    })
}

//FIREBASE

function getStored(type, collection = 'coins'){
  return db.once('value')
    .then(data => data.val() && data.val()[collection] || {})
    .then(obj => type === 'ids' ? Object.keys(obj) : Object.keys(obj).map(key => obj[key]))
}

function updateStored(data, collection = 'coins'){
  if(!data.id)
    return Promise.reject('Missing id in', data);
    
  return db.child(collection + '/' + data.id).update(data)
    .then($ => {
      console.log(`"/${collection}/${data.id}" updated in , https://coinmarketcap.com${data.link}`);
      return data;
    });
}

function removeField(coin, field){
  console.log(`"${coin.symbol}" field "${field}" removed`);
  return db.child('/coins/' + coin.id + '/' + field).remove();
}


//GETS

function setMissingCoinExtras(limit){
  return getMissingCoinExtras(limit, true)
    .then( extras => Promise.all(extras.map( extra => updateStored(extra, 'extras') )) )
}

function getMissingCoinExtras(limit, full){
  return Promise.all( [ getStored(), getStored('ids', 'extras') ])
    .then( ([coins, ids]) => coins.filter(coin => !ids.includes(coin.id)) )
    .then( coins => limit && coins.slice(0, limit) || coins )
    .then( coins => full ? coins.map(coin => Object.assign(coin, {full, limit})) : coins )
    .then( coins => !full 
      ? coins 
      : Promise.all(coins.map(
        coin => coinmarks.single(coin.link).then( extra => new Extra(Object.assign(coin, extra)) )
      )) )
}

function getMissingSupplyCoins(){
  return Promise.all( [ getStored(), coinmarks.current() ] )
    .then( ([ stored, current ]) => ({
      ids:      stored.filter(coin => !coin.supply).map(coin => coin.id),
      current:  current.map(coin => new Coin(coin))
    }))
    .then( ({ ids, current }) => current.filter(coin => coin.supply && ids.includes(coin.id)) )
}

function getMissingListedCoins(limit = 5){
  return getFieldlessCoins('listed')
    .then(coins => coins.slice(0, limit))
    .then(coins => Promise.all(coins.map(coin => 
      coinmarks.singleHistory(coin.link).then(coinHistory => Object.assign(coin, coinHistory))
    )))
}

function getMissingLatestCoins(){
  return Promise.all([ coinmarks.current(), getStored('ids') ])
    .then( ([current, ids]) => current.filter(coin => !ids.includes(coin.id) ))
}

function getFieldlessCoins(field = 'listed'){
  return getStored()
    .then( coins => coins.filter(coin => !coin[field] || coin[field] === 'NA'))
}

function getMissingRecentCoins(){
  return Promise.all( [ coinmarks.recent(), getStored('ids') ])
    .then( ([ recent, ids ]) => recent.filter(coin => !ids.includes(coin.id) ))
}

function getCoinStats(){
  return Promise.all([ 
    getStored(),
    getStored(null, 'extras'),
    coinmarks.latest(),
    getFieldlessCoins('listed'), 
    getFieldlessCoins('supply'), 
    getMissingLatestCoins(),
    getMissingSupplyCoins(),
    getMissingCoinExtras()
  ]).then( ([ stored, extras, latest, fieldlessListed, fieldlessSupply, missingLatest, missingSupply, missingExtras ]) => ({ 
    stored:   stored.length,
    extras:   extras.length,
    latest:   latest.length,
    missing: {
      latest:   missingLatest.length,
      supply:   missingSupply.length,
      extras:   missingExtras.length
    },
    fieldless: {
      listed: fieldlessListed.length,
      supply: fieldlessSupply.length
    }
  }) )
}


//MODELS
function Coin(data, update){
  this.id                 = data.id;
  this.symbol             = data.symbol;
  this.name               = data.name;
  this.timestamp          = new Date().getTime();
  this.link               = data.link || '/currencies/' + data.id;
  //this.coinmarketcaplink  = 'https://coinmarketcap.com' + data.link;
  
  //console.log(typeof data.listed);
  if(typeof data.listed !== 'undefined')
    this.listed = data.listed;
  
  if(data.supply || data.max_supply || data.total_supply || data.available_supply)
    this.supply = parseInt(data.supply || data.max_supply || data.total_supply || data.available_supply) || null;
  
  if(update)
    this.updated = new Date().getTime();
  
  return this;
}

function Response(data){
  this.ok   = true;
  this.wait = LAST_UPDATE.timestamp - new Date().getTime();
  
  if(data)
    this.data = data;
  
  return this;
}

function Extra(data){
  this.id         = data.id;
  if(data.web)
    this.website  = data.web;
  if(data.logo)
    this.logo       = data.logo;
  this.link       = data.link;
  this.timestamp  = new Date().getTime();
  this.markets    = getMarkets(data.markets);
  
  function getMarkets(markets){
    return markets.map(market => new Market(market))
      .reduce((obj, market) => {
        obj[market.id] 
          ? addExchange(obj[market.id], market) 
          : obj[market.id] = new Exchange(market);
        return obj;
      }, {});
  }
  return this;
}

function Market(data){
  this.id     = data.name.replace(/[^a-z0-9]/gi,'');
  this.name   = data.name;
  this.symbol = data.market;
  this.link   = data.marketLink;
  this.volume = data.volume && Number(data.volume.usd);
  this.price  = data.price && Number(data.price.usd);
  
  this.pc     = data.volume && data.volume.pc.trim();
  this.pc     = this.pc && Number(this.pc.replace('%', ''));
  return this;
}

function Exchange(market){
  this.name   = market.name;
  this.volume = 0;
  this.price  = 0;
  this.pc     = 0;
  this.exchanges = [];
  
  addExchange(this, market);
  return this;
}

function addExchange(ex, { volume, price, pc, link, symbol }){
  ex.exchanges.push({ link, symbol, price });
  ex.volume += volume;
  ex.pc     = Number((ex.pc + pc).toFixed(2));
  ex.price  = Number(
    (Math.round(ex.exchanges.reduce((sum, n) => sum + n.price, 0) * 1000) / 
    (ex.exchanges.length * 1000)).toFixed(4)
  );
  return ex;
}

function ranum(min, max){
  return parseInt(Math.random() * (max - min) + min)
}

// function fillField(field = 'listed'){
//   console.log(field);
//   switch(field){
//     case 'listed':
//       return getFieldlessCoins(field)
//         .then(coins => !coins.length ? Promise.resolve(coins) : coins )
//         .then(coins => 
//           Promise.all( coins.map(coin => 
//             coinmarks.singleHistory(coin.link)
//               .then(coinExtras => updateCoin(Object.assign(coin, { listed: coinExtras.history.pop().date, name: coinExtras.name})))
//           ))
//         )
//     case 'supply':
//       return Promise.all( [ getFieldlessCoins(field), coinmarks.latest() ]
//         .then(coins => !coins.length ? Promise.resolve(coins) : coins ))
//         // .then(coins => 
//         //   Promise.all( coins.map(coin => 
//         //     coinmarks.singleHistory(coin.link)
//         //       .then(coinExtras => updateCoin(Object.assign(coin, { listed: coinExtras.history.pop().date, name: coinExtras.name})))
//         //   ))
//         // )
//   }
    
// }

// function removeCoinField(field){
//   return getStored()
//     .then(coins => coins.map(coin => removeField(coin, field)))
// }

// function setRecentCoins(){
//   return getMissingCoins()
//     .then( ({notlisted, newest}) => Promise.all(newest.map(updateCoin)).then(newCoins => notlisted))
//     .then( notlisted => Promise.all(notlisted.map(updateCoin)))
// }

// function addNotStoredCoins(){
//   return getMissingAllCoins()
//     .then(coins => coins.map(updateCoin))
// }

// function setStoredCoinsDate(){
//   getStored()
//     //.then(coins => coins.filter(coin => !coin.link))
//     .then(coins => coins.filter(coin => !coin.listed))
//     .then(coins => coins.length ? coins : Promise.reject('all stored coins now have listed value'))
//     .then(coins => coins.slice(0, 1))
//     .then(coins => collectHistory(coins))
//     //.then(console.log)
//     .then(coins => setStoredCoinsDate());
// }

// function collectHistory(coins){
//   return Promise.all(coins.map(coin => {
//     if(!coin.link) return Promise.reject(new Error(coin.symbol + ' does not have any link'));
//     return coinmarks.singleHistory(coin.link)
//       .then(coinExtras => {
//         coin.listed = coinExtras.history.pop().date;
//         coin.name   = coinExtras.name;
//         return updateCoin(coin);
//       })
//   }))
// }

// coinmarks.exchange('/exchanges/coinexchange/').then(json => {
//   fs.writeFile('./files/content.json', JSON.stringify(json, null, 2) , 'utf-8');
// });

// function checkCoins(){
//   let data = content.slice(0);

//   let bittrex = [];
  
//   data.map(coin => {
//     coin.markets.forEach(market => {
//       coin.exchanges = lodash.union(coin.exchanges || [], [market.name]);
//     });
//     coin.exchanges.indexOf('Bittrex') >= 0 && bittrex.push({ name: coin.name, symbol: coin.symbol, price: coin.price.usd });
//     return coin;
//   });
  
//   console.log(bittrex);
// }


//getStored().then(coins => fs.writeFile('./files/coins.json', JSON.stringify({coins}, null, 2) , 'utf-8'))

//var newPostKey = db.ref('/').child('coins').push().key
//.then(coins => coins.map(coin => removeField(coin.symbol, 'link')))



//setStoredCoinsDate();
//setRecentCoins();
//removeCoinField('coinmarketcaplink');
//getStored().then(console.log);


// function isUpdatedDayAgo(coin){
//   return !coin.updated || ((new Date().getTime() - coin.updated) > (1000 * 60 * 60 * 24));
// }

// function isCompleteCoin(coin){
//   return coin.supply && coin.listed && coin.listed !== 'NA';
// }

// function SmartCoin(coin){
//   let calls = [];
  
//   if(isCompleteCoin(coin))
//     return {id: coin};
  
//   !coin.supply && calls.push(coinmarks.singleLatest(coin.id).then(res => {
//     return typeof res === 'object' ? Promise.reject(res.error) : res.shift();
//   }));
//   !coin.listed && calls.push(coinmarks.singleHistory(coin.link));
    
//   return Promise.all(calls)
//     .then(arr => arr.reduce((obj = {}, next) => Object.assign(obj, next)))
//     .then(stats => updateCoin(Object.assign(coin, stats), true))
    
//   /** TODO!!! FINISH STORING last_updated field & supply 
//     To  many calls bug!!!
//   **/
// }

//coinmarks.all().then(console.log)





// fetch('https://coin.fyi/news/general?news_count=5&coin_id=810')
//   .then(res => res.json())
//   .then(console.log);




