const router    = global.express.Router();
const fs        = require('fs');
//const content   = require('../files/content.json');
const lodash    = require('lodash');
const fetch     = require('node-fetch');
const cheerio   = require('cheerio');

// ROUTES
router.get('/', (req, res) => scrapeTest().then(obj => res.json(obj)));
scrapeTest();
module.exports = router;

function scrapeTest(html){
  html = html || fs.readFileSync('test/test.html', 'utf8');
  let schema = {
    name:     'h1, eq:0, text, trim',
    desc:     'p, eq:1, text, trim',
    title:    'title',
    button:   [ 'button.navbar-toggle.collapsed', 'attr:data-target' ],
    markets:  [ 'table#currencies-all tbody tr', 'slice:-5', {
      name: 'td.currency-name span.currency-symbol a',
      price: 'td a.price, attr:data-btc'
    }]
  };
  let data = scrapis(schema, cheerio.load(html));
  return Promise.resolve({ ok: true, schema, data });
}

function scrapis(schemas, $, root){
  let data = {};
  for(var key in schemas){
    data[key] = getValues(schemas[key], root)
  }

  return data;

  function getValues(schema, elem){
    schema = cloneSchema(schema);
    schema = schema.selector && schema || {
      selector:  typeof schema === 'string' && schema   || schema.shift(),
      functions: !schema.length && ['text', 'trim'] || schema.filter(fn => typeof fn === 'string'),
      children:  typeof schema !== 'string' && schema.find(fn => typeof fn === 'object'),
    };

    //.map(v => v.split(','))
    schema.functions = schema.functions.map(function(fn){
      return fn.indexOf(':') && fn.split(':')
    });
    //console.log(schema.functions);
    schema.selector = elem ? $(elem).find(schema.selector) : $(schema.selector);
    //console.log(schema.functions);
    const self = this;
    elem = schema.functions.length
      ? schema.functions.reduce((val, fn) => val[fn[0]](fn[1]), schema.selector)
      : schema.selector;

    if(!schema.children)
      return elem;

    //console.log(typeof elem, elem.length, schema.children);
    //console.log('children', schema.children);
    return elem.map((i, el) => scrapis(schema.children, $, el)).get()//.map(el => scrapis(schema.children, $, el) $(el).find('td.currency-name span.currency-symbol a').text()

  }

  function cloneSchema(schema){
    if(typeof schema === 'string')
      schema = schema.split(',').map(v => v.trim());
    if(schema instanceof Object)
      return JSON.parse(JSON.stringify(schema));
    if(schema instanceof Array)
      return schema.filter();
    return schema;
  }
  //console.log($('h1').eq(0).text());
}
