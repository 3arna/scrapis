function pageFunction(context){
    // Libs
    var $    = context.jQuery;
    var _    = context.underscoreJs;
    // Consts
    var URL  = context.request.loadedUrl;
    var DATA = context.request.interceptRequestData;

    return context.request.label === 'main'
        ? mainPage(10)
        : currencyPage(DATA, 3);

    // Methods

    function currencyPage(coin, marketLimit){
        var markets = [];
        // elems
        var ELEM_IMG        = $('img.currency-logo-32x32');
        var ELEM_WEB        = $('ul.list-unstyled span.glyphicon-link').eq(0).next();
        var ELEM_MARKETS    = $('table#markets-table tbody tr');

        ELEM_MARKETS.map(extractMarket);

        var result = {
            name:       ELEM_IMG.attr('alt'),
            website:    ELEM_WEB.attr('href'),
            logo:       ELEM_IMG.attr('src'),
            markets:    marketLimit && markets.slice(0, marketLimit) || markets
        };
        return _.assign(coin, result);

        function extractMarket(){
            var elem = $(this);
            var ELEM_EXCHANGE   = elem.find('td a').eq(0);
            var ELEM_PAIR       = elem.find('td a').eq(1);
            var ELEM_VOL        = elem.find('td span.volume');
            var ELEM_PRICE      = elem.find('td span.price');
            var ELEM_PC         = elem.find('td span[data-format-percentage]');

            markets.push({
                name:       ELEM_EXCHANGE.text(),
                exchange:   ELEM_EXCHANGE.attr('href'),
                pair:       ELEM_PAIR.text(),
                link:       ELEM_PAIR.attr('href'),
                price: {
                    btc: ELEM_PRICE.attr('data-btc'),
                    usd: ELEM_PRICE.attr('data-usd')
                },
                vol: {
                    btc: ELEM_VOL.attr('data-btc'),
                    usd: ELEM_VOL.attr('data-usd'),
                    pc:  parseInt(ELEM_PC.attr('data-format-value'))
                }
            });
        }
    }

    function mainPage(limit, skipEnque){
        var results         = [];
        // Selector list
        var SELECTOR_TR     = 'table#currencies tbody tr';
        var SELECTOR_SYMBOL = 'span.currency-symbol';
        var SELECTOR_LINK   = 'a.currency-name-container';
        var SELECTOR_CAP    = 'td.market-cap';
        var SELECTOR_VOL    = 'a.volume';
        var SELECTOR_SUP    = 'td.circulating-supply a';
        var SELECTOR_CHANGE = 'td.percent-change';
        var SELECTOR_GRAPH  = 'img.sparkline';
        // loop through tr elements
        $(SELECTOR_TR).each(extractCoin);
        // filtering results by limit
        results = limit && results.slice(0, limit) || results;
        // skipping enque process if skipEnque is set to true
        !skipEnque && results.map(enque);
        return context.skipOutput() || results;

        function extractCoin(){
            var elem = $(this);
            results.push({
                id:     elem.attr('id').replace('id-', ''),
                name:   elem.find(SELECTOR_LINK).text(),
                symbol: elem.find(SELECTOR_SYMBOL).text(),
                link:   elem.find(SELECTOR_LINK).attr('href'),
                sup:    elem.find(SELECTOR_SUP).attr('data-supply'),
                change: elem.find(SELECTOR_CHANGE).attr('data-percentusd'),
                graph:  elem.find(SELECTOR_GRAPH).attr('src'),
                cap: {
                    btc: elem.find(SELECTOR_CAP).attr('data-btc'),
                    usd: elem.find(SELECTOR_CAP).attr('data-usd')
                },
                vol: {
                    btc: elem.find(SELECTOR_VOL).attr('data-btc'),
                    usd: elem.find(SELECTOR_VOL).attr('data-usd')
                }
            });
        }

        function enque(coin){
            context.enqueuePage({
                url: URL + coin.link,
                interceptRequestData: coin
            });
            return coin;
        }
    }
}
