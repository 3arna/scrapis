const express   = global.express = require('express');
const app       = express();

//Routers | Func
app.use('/scrapis',   require('./func/scrapis.func.js'));
app.use('/coinmarks', require('./func/coinmarks.func.js'));

//Route list
app.get('/', (req, res) => res.json(utils.getRoutes()));

app.listen(process.env.PORT || 3001, () => console.log('Express APP port ' + process.env.PORT || 3001));


const utils = {
  getRoutes: () => {
    //Router list
    let routelist = [], routerPath;
    app._router.stack.filter(route => route.name === 'router').map(route => {
      routerPath = route.regexp.toString().split('\\')[1];
      route.handle.stack.slice().forEach(r => routelist.push({
          path:   routerPath + r.route.path,
          method: r.route.methods.get && 'GET' || 'OTHER',
      }));
      return route;
    });
    return routelist;
  }
}
